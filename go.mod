module gitea.com/macaron/macaron

go 1.11

require (
	gitea.com/macaron/inject v0.0.0-20160627170012-d8a0b8677191
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/go-macaron/inject v0.0.0-20160627170012-d8a0b8677191 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	gopkg.in/ini.v1 v1.44.0
)
